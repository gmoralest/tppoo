# TP3_POO

## About : POO.py :

This script creates a Linked list from a txt file, in which the user is capable of adding new 
elements into the list, removing, indexing, listing all the elements of the list, 
insert a complete list into the linked list.
The programs recover all the numbers in the txt file and insert them, in a order way
if the user wants it, to a linked list, that after that, can be modified.

## Features :

**Main Feature** :
- Create and modify a Linked list from a txt file. 

**Optional Features** :
-i -> Input file, from which all the numbers will be recover 
--order -> allows the user to obtain a sorted linked list


## Installation :

To install this script use the following commands :

```bash
mkdir ~/Desktop/MORALES/
cd ~/Desktop/MORALES/
git clone https://gitlab.com/gmoralest/tppoo
```
==> You're now good to go!

## Usage :

For information on using the script you can type the following commands :

```bash
cd ~/Desktop/MORALES/
python3 POO.py -h
```

## Example of use :

python3 POO.py -i exemple.txt 
python3 POO.py -i exemple.txt --order


## Authors :

**Gabriel Morales**
