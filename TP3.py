from re import A
from turtle import color, window_height


class Animal:
    def __init__(self, color, weight, diet, childs = None):
        self.color = color
        self.weight = weight
        self.diet = diet
        if (childs != None):
            for child in childs:
                child.set_parents(self)
        self.childs = childs
    
    def set_parents(self,parent):
        self.parent = parent


    def get_color(self):
        return self.color

    def set_color(self, new_color):
        self.color = new_color

    def set_diet(self, new_diet):
        self.diet = new_diet

    def get_diet(self):
        return self.diet

    def eat(self, weight):
        self.weight += weight / 10

    def __str__(self):
        string =  "Animal of color " + self.color + " and weight " + str(self.weight) + " that is " + self.diet
        if self.parent != None:
            string =+ " (that my parent is color " + self.parent.color + ") "
        if self.childs != None:
            string += "avec " + str(len(self.childs)) + " enfants: \n"
            for child in self.childs:
                string += "\t" + str(child) + "\n"
        return string
    
    def add_child(self, color):
        child = Animal(color, 1, self.diet)
        child.set_parents(self)
        if self.childs == None:
            self.childs = []
        self.childs.append(child)


class Jaguar(Animal):
    def __init__(self, color, weight):
        # call the constructor from a superior class
        super().__init__(color, weight, "carnivore ")

    def set_diet(self, diet):
        # return super().set_diet(diet)
        print("Its not possible to change the diet of the Jaguar")
        # sets the diet of the Jaguar

    def __str__(self):
        return "Jaguar of color " + self.color + " and weight " + str(self.weight) + " that is " + self.diet


#add 2 childs to an animal
animal1_1 = Animal("green", 5, "carnivore")
animal1_2 = Animal("yellow", 5, "carnivore")
childs = [animal1_1,animal1_2]
animal1 = Animal("blue", 5, "carnivore", childs)
animal1.add_child("violet")
animal1 = Animal("blue", 5, "carnivore", childs)

#create a Jaguar from an animal
animal2 = Animal("red", 5, "carnivore")
animal3 = Jaguar("red", 5)
print(animal3.diet)

#set weight base on a method
print(animal1.weight)
animal1.eat(10)
print(animal1.weight)


print(animal1.color)
print(animal1.get_color())
animal3.set_diet("vegan")
print(animal1)
print(animal3)

#change color
print(animal2.color)
animal2.set_color("orange")
print(animal2.get_color())

#impossible to change diet
print(animal3.get_diet())
animal3.set_diet("vegan")
print(animal3.get_diet())

print(animal2.get_diet())
animal2.set_diet("vegan")
print(animal2.get_diet())

#add a child of a child
animal1.childs[2].add_child("black")
print(animal1.childs[2])

# el interes del metodo, es que permite controla como ingresamos informacion en los objetos (una persona, para aumneta el peso el animal
# debe usar la funcion eat, para ganar el peso determinado por el desarrollador, y no meter el peso que desea.)
